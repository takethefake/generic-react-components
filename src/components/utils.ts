export const createPropsGetter = <DP extends object>(defaultProps: DP) => {
  return <P extends Partial<DP>>(props: P) => {
    //we are extracting default Props from component Props api type
    type PropsExcludingDefault = Pick<P, Exclude<keyof P, keyof DP>>;
    // we are recreating our props definition by creating an intersection type
    // between  Props without Defaults and NonNullable DefaultProps
    type RecomposedProps = DP & PropsExcludingDefault;
    //we are returning the same props that we got as argument - identity function.
    // Also we are turning off compiler and casting  the type to our new  RecomposedProps type
    return (props as any) as RecomposedProps;
  };
};

// tslint:disable
export const noop = () => {};
// tslint:enable
