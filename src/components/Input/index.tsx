import * as React from "react";
import { noop, createPropsGetter } from "../utils";

interface IInput {
  children: (props: InjectedProps) => JSX.Element;
  defaultValue?: string;
  disabled?: boolean;
  label?: string;
  onBlur?: (event: React.SyntheticEvent<HTMLInputElement>) => any;
  onFocus?: (event: React.SyntheticEvent<HTMLInputElement>) => any;
  placeholder?: string;
  validatorGroup?: IValidatorGroupItem[];
}

export type IInputProp = {
  disabled: boolean;
  label: string;
  onBlur: (event: React.ChangeEvent<HTMLInputElement>) => void;
  onChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
  onFocus: (event: React.FocusEvent<HTMLInputElement>) => void;
  placeholder: string;
  type: string;
  value: string;
} & Partial<DefaultProps>;

type DefaultProps = Readonly<typeof defaultProps>;

const defaultProps = {
  defaultValue: "" as string,
  disabled: false as boolean,
  label: "" as string,
  onBlur: noop as Function,
  onFocus: noop as Function,
  placeholder: "" as string
};
const getProps = createPropsGetter(defaultProps);

export interface InjectedProps {
  inputProps: IInputProp;
  error?: IValidatorGroupItem[];
}

export interface IValidatorGroupItem {
  /**
   * A value describing what the validator validates.
   * Example: 'Needs at least 8 characters'
   */
  description: string;
  /**
   * Returns true if the validation requirements are met
   */
  validator: (value: string) => boolean;
}

export interface IState {
  error: IValidatorGroupItem[];
  focused: boolean;
  value: string;
  wasFocusedOnce: boolean;
}

class Input extends React.Component<IInput, IState> {
  static defaultProps = defaultProps;
  public state = {
    error: [],
    focused: false,
    value: this.props.defaultValue ? this.props.defaultValue : "",
    wasFocusedOnce: false
  };
  //tslint:disable
  constructor(props: IInput) {
    super(props);
  }

  protected hasErrors = (value: string): IValidatorGroupItem[] => {
    if (this.props.validatorGroup) {
      return this.props.validatorGroup.filter(
        (validatorGroupItem: IValidatorGroupItem) =>
          !validatorGroupItem.validator(value)
      );
    }
    return [];
  };

  public handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const curVal = event.currentTarget.value;
    const error = this.hasErrors(curVal);
    this.setState({ value: event.currentTarget.value, error });
  };
  public onFocus = (event: React.SyntheticEvent<HTMLInputElement>) => {
    const curVal = event.currentTarget.value;
    this.setState({
      error: this.state.wasFocusedOnce ? this.hasErrors(curVal) : [],
      focused: true,
      wasFocusedOnce: true
    });
    this.props.onFocus ? this.props.onFocus(event) : () => {};
  };

  public onBlur = (event: React.SyntheticEvent<HTMLInputElement>) => {
    const curVal = event.currentTarget.value;
    this.setState({
      focused: false,
      error: this.hasErrors(curVal)
    });
    this.props.onBlur ? this.props.onBlur(event) : () => {};
  };

  public render() {
    const { disabled, label, placeholder } = getProps(this.props);
    const inputProps = {
      disabled,
      label,
      onChange: this.handleChange,
      onFocus: this.onFocus,
      onBlur: this.onBlur,
      placeholder,
      type: "text",
      value: this.state.value
    };

    return this.props.children({ inputProps, error: this.state.error });
  }
}

export { Input };
