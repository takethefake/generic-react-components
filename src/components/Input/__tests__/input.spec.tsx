import * as React from "react";
import { shallow, mount } from "enzyme";

import { Input, IState, InjectedProps } from "../index";

describe("InputComponent", () => {
  describe("handles the disabledProperty properly", () => {
    test("with defaultProps", () => {
      shallow(
        <Input>
          {(props: InjectedProps) => {
            expect(props.inputProps.disabled).toBe(false);
            return <div />;
          }}
        </Input>
      );
    });
    test("with definedProp", () => {
      shallow(
        <Input disabled={true}>
          {(props: InjectedProps) => {
            expect(props.inputProps.disabled).toBe(true);
            return <div />;
          }}
        </Input>
      );
      shallow(
        <Input disabled={false}>
          {(props: InjectedProps) => {
            expect(props.inputProps.disabled).toBe(false);
            return <div />;
          }}
        </Input>
      );
    });
  });
  describe("handles the defaultValue", () => {
    test("with defaultProps", () => {
      shallow(
        <Input>
          {(props: InjectedProps) => {
            expect(props.inputProps.value).toBe("");
            return <div />;
          }}
        </Input>
      );
    });
    test("with definedProp", () => {
      const teststring = "asdas83r1$%&";
      shallow(
        <Input defaultValue={teststring}>
          {(props: InjectedProps) => {
            expect(props.inputProps.value).toBe(teststring);
            return <div />;
          }}
        </Input>
      );
    });
  });
  describe("handles the placeholder", () => {
    test("with defaultProps", () => {
      shallow(
        <Input>
          {(props: InjectedProps) => {
            expect(props.inputProps.placeholder).toBe("");
            return <div />;
          }}
        </Input>
      );
    });
    test("with definedProp", () => {
      const teststring = "asdas83r1$%&";
      shallow(
        <Input placeholder={teststring}>
          {(props: InjectedProps) => {
            expect(props.inputProps.placeholder).toBe(teststring);
            return <div />;
          }}
        </Input>
      );
    });
  });
  describe("accept value changes", () => {
    test("with definedProp", () => {
      const teststring = "asdas83r1$%&";
      const dom = mount(
        <Input>
          {(props: InjectedProps) => {
            return <input {...props.inputProps} />;
          }}
        </Input>
      );
      const input = dom.find("input");
      input.simulate("change", { currentTarget: { value: teststring } });
      expect((dom.state() as IState).value).toBe(teststring);
      expect((input.props() as InjectedProps).inputProps.value).toBe(
        teststring
      );
    });
  });
});
