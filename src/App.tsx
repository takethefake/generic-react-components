import * as React from "react";
import "./App.css";

import { InjectedProps, Input } from "./components/Input";
import logo from "./logo.svg";

class App extends React.Component {
  public render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <p className="App-intro">
          <Input
            validatorGroup={[
              {
                description: "Cant be empty",
                validator: value => value !== ""
              },
              {
                description: "has to contain a",
                validator: value => value.indexOf("a") >= 0
              }
            ]}
          >
            {(props: InjectedProps) => {
              const errorSpan = props.error!.map(error => (
                <span>{error.description}</span>
              ));
              return (
                <div
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    width: "300px"
                  }}
                >
                  <input {...props.inputProps} />
                  {errorSpan}
                </div>
              );
            }}
          </Input>
        </p>
      </div>
    );
  }
}

export default App;
